import { Publisher, ScreenshotFinishedEvent, Subjects } from "@imscreenshots/common";

export class ScreenshotFinishedPublisher extends Publisher<ScreenshotFinishedEvent> {
  subject: Subjects.ScreenshotFinished = Subjects.ScreenshotFinished
}