import { Subjects } from './subjects';

export interface ScreenshotFinishedEvent {
  subject :Subjects.ScreenshotFinished,
  data: {
    id: string;
    cloundinary_url: string;
    cloundinary_id: string;
    cloundinary_version: string;
    width: number;
    height: number;
    bytes: number;
    format: string;
  }
}